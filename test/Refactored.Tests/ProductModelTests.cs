using System;
using Xunit;
using Refactored.Models;

namespace Refactored.Tests 
{
    public class ProductModelTests : IDisposable
    {
        Product product;

        public ProductModelTests()
        {
            product = new Product{
                Id = Guid.NewGuid(),
                Name = "Broccoli",
                Description = "Green vegetable",
                Price = 1,
                DeliveryPrice = 0.5M
            };
        }

        public void Dispose()
        {
            product = null;
        }

        [Fact]
        public void CanChangeName() 
        {
            //Arrange
            //Act
            product.Name = "Bok Choi";

            Assert.Equal("Bok Choi", product.Name); 
        }
    }
}