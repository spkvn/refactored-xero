using System;
using System.Collections.Generic;
using Xunit;
using Refactored.Controllers;
using Refactored.Data;
using Refactored.Dtos;
using Refactored.Models;
using Refactored.Profiles;
using Microsoft.AspNetCore.Mvc;

namespace Refactored.Tests
{
    public class ProductsControllerTests : IDisposable
    {
        Mock<IProductRepo> mockRepo;
        ProductsProfile autoMapProfile;
        MapperConfiguration mapperConf;
        IMapper mapper;
        public ProductsControllerTests()
        {
            mockRepo = new Mock<IProductRepo>();
            autoMapProfile = new ProductsProfile();
            mapperConf = new MapperConfiguration(cfg => cfg.AddProfile(autoMapProfile));
            mapper = new Mapper(mapperConf);
        }

        public void Dispose()
        {
            mockRepo = null;
            autoMapProfile = null;
            mapperConf = null;
            mapper = null;
        }

        [Fact]
        public void GetProducts_Returns200OK_WithNullSearchParam() 
        {
            //Arrange
            mockRepo.Setup(repo => repo.GetAllProducts()).Returns(GetProducts(0));
            var controller = new ProductsController(mockRepo.Object, mapper);

            //Act
            var result = controller.GetProducts(null);

            //Assert
            Assert.IsType<OkObjectResult>(result.Result);
        }

        [Fact]
        public void GetProducts_Returns200Ok_WithSearchParam() 
        {
            //Arrange
            mockRepo.Setup(repo => repo.GetProductsByName("not null")).Returns(GetProducts(0));
            var controller = new ProductsController(mockRepo.Object, mapper);

            //Act
            var result = controller.GetProducts("not null");

            //Assert
            Assert.IsType<OkObjectResult>(result.Result);
        }

        [Fact]
        public void GetProducts_Returns200OkOfCorrectSize_WithNullSearchParam() 
        {
            //Arrange
            mockRepo.Setup(repo => repo.GetAllProducts()).Returns(GetProducts(2));
            var controller = new ProductsController(mockRepo.Object, mapper);

            //Act
            var result = controller.GetProducts(null);

            //Assert
            Assert.IsType<OkObjectResult>(result.Result);
            
            var okResult = result.Result as OkObjectResult;
            var productsResponse = okResult.Value as ProductsResponse;

            Assert.Equal(2, productsResponse.Items.Count);
        }

        [Fact]
        public void GetProductById_Returns200Ok_WhenDbReturnsProduct()
        {
            // Arrange
            var productId = Guid.NewGuid();
            mockRepo.Setup(repo => repo.GetProductById(productId)).Returns(GetProduct(productId));
            var controller = new ProductsController(mockRepo.Object, mapper);
            
            // act
            var result = controller.GetProductById(productId);

            // assert
            Assert.IsType<OkObjectResult>(result.Result);
        }
        
        [Fact]
        public void GetProductById_Returns404NotFound_WhenDbReturnsNoProduct()
        {
            // Arrange
            var productId = Guid.NewGuid();
            mockRepo.Setup(repo => repo.GetProductById(productId)).Returns((Product)null);
            var controller = new ProductsController(mockRepo.Object, mapper);
            
            // act
            var result = controller.GetProductById(productId);

            // assert
            Assert.IsType<NotFoundResult>(result.Result);
        }

        [Fact]
        public void CreateProduct_Returns200OkWithCorrectResponse_WhenValidRequestSent() 
        {
            // Arrange 
            var controller = new ProductsController(mockRepo.Object, mapper);
            var createRequest = new ProductRequest{
                Name = "mock",
                Description = "mock",
                Price = 0,
                DeliveryPrice = 1
            };
            
            // act 
            var result = controller.CreateProduct(createRequest);

            // assert
            Assert.IsType<CreatedAtRouteResult>(result.Result);
            Assert.IsType<ActionResult<ProductResponse>>(result);
        }

        [Fact]
        public void UpdateProduct_Returns200Ok_WhenValidRequestSent() 
        {
            // Arrange 
            var productId = Guid.NewGuid();
            mockRepo.Setup(repo => repo.GetProductById(productId)).Returns(GetProduct(productId));
            var controller = new ProductsController(mockRepo.Object, mapper);
            var updateRequest = new ProductRequest{
                Name = "mock",
                Description = "mock",
                Price = 0,
                DeliveryPrice = 1
            };

            // act
            var result = controller.UpdateProduct(productId, updateRequest);

            // assert
            Assert.IsType<OkResult>(result);
        }



        [Fact]
        public void UpdateProduct_Returns404NotFound_WhenNoProductReturned() 
        {
            // Arrange 
            var productId = Guid.NewGuid();
            mockRepo.Setup(repo => repo.GetProductById(productId)).Returns((Product) null);
            var controller = new ProductsController(mockRepo.Object, mapper);

            // act
            var result = controller.UpdateProduct(productId, new ProductRequest());

            // assert
            Assert.IsType<NotFoundResult>(result);
        }
         
        [Fact]
        public void DeleteProduct_Returns200Ok_WhenValidRequestSent() 
        {
            // Arrange 
            var productId = Guid.NewGuid();
            mockRepo.Setup(repo => repo.GetProductById(productId)).Returns(GetProduct(productId));
            var controller = new ProductsController(mockRepo.Object, mapper);

            // act
            var result = controller.DeleteProduct(productId);

            // assert
            Assert.IsType<OkResult>(result);
        }

        [Fact]
        public void DeleteProduct_Returns404NotFound_WhenNoProductReturned() 
        {
            // Arrange 
            var productId = Guid.NewGuid();
            mockRepo.Setup(repo => repo.GetProductById(productId)).Returns((Product) null);
            var controller = new ProductsController(mockRepo.Object, mapper);

            // act
            var result = controller.DeleteProduct(productId);

            // assert
            Assert.IsType<NotFoundResult>(result);
        }

        private List<Product> GetProducts(int n) 
        {
            var products = new List<Product>();
            for(int i = 0; i < n; i++) {
                products.Add(GetProduct(Guid.NewGuid()));
            }
            return products;
        }

        private Product GetProduct(Guid id)
        {
            return new Product{
                Id = id,
                Name = "Abc",
                Description = "123",
                Price = 123,
                DeliveryPrice = 321
            };
        }
    }
}