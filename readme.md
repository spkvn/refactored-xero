# refactored-proj

Hey team. I chose to re-write the project which was provided, as I didn't know C# or .net core, I thought it would be the most expiedient way to fix up the project. 

There's two models and controllers to handle the endpoints listed in your readme, and I've created a postman collection for each of them. You'll need to swap out the GUIDs in most of the postman requests, as they're generated each time the test data is generated.

Some very brief unit testing on one controller to demonstrate I know how things like mocking work :)

I ran into some issues with SQLite and EntityFramework when searching by Guid. The EF.Sqlite package appears to interpret them as strings, which caused problems matching, and led to this [unanswered stack overflow question](https://stackoverflow.com/questions/68743538/entity-framework-firstordefault-predicate-not-returning-expected-results/68752072#68752072)

This whole database fiasco caused me to swap in a postgres container, I've got explanation of how to set it up below. 

Fun little project, hope you like it.

Thanks,
\
Kevin

---

**Running the API**

1. Need `dotnet` and `docker`
2. move `secrets.json` to `~/.microsoft/usersecrets/refactored-secrets/secrets.json` on your machine. 
\
I know it's a bad idea to save these to repo but it's a containerised local db anyway so no worries.

**Start the database**

This downloads [postgres](https://hub.docker.com/_/postgres) from dockerhub, runs EF migrations, and then executes some test data inserts.

```
cd src/Refactored 
./db.sh start 
./db.sh load-test-data
```

**Start the API**

```
cd src/Refactored 
dotnet run
```

**Run tests** 

```
cd test/Refactored.Tests
dotnet test
```