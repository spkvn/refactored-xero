using AutoMapper;
using Refactored.Models;
using Refactored.Dtos;

namespace Refactored.Profiles 
{
    public class ProductsProfile : Profile 
    {
        public ProductsProfile() 
        {
            CreateMap<ProductRequest, Product>();
            CreateMap<Product, ProductResponse>();
            CreateMap<ProductOptionRequest, ProductOption>();
            CreateMap<ProductOption, ProductOptionResponse>();
        }
    }
}