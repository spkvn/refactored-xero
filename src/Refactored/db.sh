#!/usr/bin/env bash

start () {
    echo "Creating DB"
    docker run --name refactored-db -e POSTGRES_PASSWORD=refactored-pass -p 5432:5432 -d postgres
    echo "Running migrations"
    dotnet ef database update
}

stop () {
    docker rm -f $(docker ps -f name=refactored-db -q) > /dev/null
}

load_test_data() {
    docker cp test-data-create.sql refactored-db:/test-data-create.sql
    docker exec -u postgres refactored-db psql postgres postgres -f /test-data-create.sql 
    docker exec -u postgres refactored-db psql postgres postgres -c "select * from \"Products\";"
    docker exec -u postgres refactored-db psql postgres postgres -c "select * from \"ProductOptions\";"
}


case "$1" in
    "start") 
        start
        ;;
    "stop") 
        stop
        ;;
    "load-test-data")
        load_test_data
        ;;
    *) 
        echo "usage: $0 (start|stop|load-test-data)"
        exit 1;;
esac
