CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

insert into "Products" ("Id", "Name", "Description", "Price", "DeliveryPrice") 
values(uuid_generate_v4(),'Broccoli','Green vegetable', 3.50, 0.5);
insert into "Products" ("Id", "Name", "Description", "Price", "DeliveryPrice") 
values(uuid_generate_v4(),'Sweet Potato','Big root veg', 2.50, 0.5);
insert into "Products" ("Id", "Name", "Description", "Price", "DeliveryPrice") 
values(uuid_generate_v4(),'Chickpeas','Green legume', 4, 0.5);
insert into "Products" ("Id", "Name", "Description", "Price", "DeliveryPrice") 
values(uuid_generate_v4(),'Capsicum','R/G/Y fruit', 4, 0.5);
insert into "Products" ("Id", "Name", "Description", "Price", "DeliveryPrice") 
values(uuid_generate_v4(),'Maple Syrup','Sweet tree juice', 4, 1);


insert into "ProductOptions" ("Id", "Name", "Description", "ProductId")
select 
    uuid_generate_v4(), 'Small', '100g', "Id"
from "Products"
where "Name" = 'Broccoli';

insert into "ProductOptions" ("Id", "Name", "Description", "ProductId")
select 
    uuid_generate_v4(), 'Medium', '500g', "Id"
from "Products"
where "Name" = 'Broccoli';

insert into "ProductOptions" ("Id", "Name", "Description", "ProductId")
select 
    uuid_generate_v4(), 'Large', '1kg', "Id"
from "Products"
where "Name" = 'Broccoli';