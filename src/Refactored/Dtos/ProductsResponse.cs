using System.Collections.Generic;
using Refactored.Models;

namespace Refactored.Dtos
{
    public class ProductsResponse
    {
        public ProductsResponse() 
        {
            Items = new List<ProductResponse>();
        }
        public List<ProductResponse> Items {get; set;}
    }
}