using System;

namespace Refactored.Dtos 
{
    public class ProductOptionResponse
    {
        public Guid Id {get; set;}
        public string Name {get; set;}
        public string Description {get; set;}

    }
}