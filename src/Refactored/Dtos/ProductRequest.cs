using System.ComponentModel.DataAnnotations;

namespace Refactored.Dtos 
{
    public class ProductRequest 
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public decimal Price { get; set; }
        [Required]
        public decimal DeliveryPrice { get; set; }

    }
}