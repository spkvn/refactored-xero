using System;
using System.ComponentModel.DataAnnotations;

namespace Refactored.Dtos 
{
    public class ProductResponse 
    {
        public Guid Id {get; set;}
        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public decimal Price { get; set; }
        [Required]
        public decimal DeliveryPrice { get; set; }

    }
}