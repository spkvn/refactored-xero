using System;
using System.ComponentModel.DataAnnotations;

namespace Refactored.Dtos 
{
    public class ProductOptionRequest
    {
        [Required]
        public Guid ProductId {get; set;}
        [Required]
        public string Name {get; set;}
        [Required]
        public string Description {get; set;}

    }
}