using System.Collections.Generic;
using Refactored.Models;

namespace Refactored.Dtos
{
    public class ProductOptionsResponse
    {
        public ProductOptionsResponse() 
        {
            Items = new List<ProductOptionResponse>();
        }
        public List<ProductOptionResponse> Items {get; set;}
    }
}