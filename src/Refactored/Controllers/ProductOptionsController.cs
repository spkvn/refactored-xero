using AutoMapper;
using System;
using System.Collections.Generic;
using Refactored.Models;
using Refactored.Dtos;
using Refactored.Data;
using Microsoft.AspNetCore.Mvc;
 
namespace Refactored.Controllers
{
    [Route("api/products/{productId}")]
    [ApiController]
    public class ProductOptionsController : ControllerBase
    {
        private readonly IProductOptionRepo _repository;
        private readonly IMapper _mapper;

        public ProductOptionsController(IProductOptionRepo repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        [HttpGet("options")]
        public ActionResult<ProductOptionsResponse> GetProductOptions(Guid productId)
        {
            var options = _repository.GetProductOptionsByProductId(productId);
            return Ok(prepareOptionsResponse(options));
        }

        [HttpGet("options/{id}", Name="GetByProductOptionByOptionId")]
        public ActionResult<ProductOptionResponse> GetByProductOptionByOptionId(Guid productId, Guid id)
        {
            var option = _repository.GetProductOptionsByProductIdAndOptionId(productId, id);
            if (option == null) {
                return NotFound();
            }
            return Ok(_mapper.Map<ProductOptionResponse>(option));
        }

        [HttpPost("options")]
        public ActionResult<ProductOptionResponse> CreateOption(Guid productId, ProductOptionRequest createRequest)
        {
            var option = _mapper.Map<ProductOption>(createRequest);
            option.ProductId = productId;
            _repository.CreateProductOption(option);
            _repository.SaveChanges();
            return CreatedAtRoute(nameof(GetByProductOptionByOptionId), new {productId = productId, id = option.Id}, option);
        }

        [HttpPut("options/{id}")]
        public ActionResult UpdateOption(Guid productId, Guid id, ProductOptionRequest updateRequest)
        {
            var option = _repository.GetProductOptionsByProductIdAndOptionId(productId, id);
            if (option == null) 
            {
                return NotFound();
            }
            _mapper.Map(updateRequest, option);
            option.ProductId = productId;
            _repository.SaveChanges();
            return Ok();
        }

        [HttpDelete("options/{id}")]
        public ActionResult DeleteOption(Guid productId, Guid id)
        {
            var option = _repository.GetProductOptionsByProductIdAndOptionId(productId, id);
            if (option == null) 
            {
                return NotFound();
            }
            _repository.DeleteProductOption(option);
            _repository.SaveChanges();
            return Ok();
        }

        private ProductOptionsResponse prepareOptionsResponse(IEnumerable<ProductOption> options)
        {
            var response = new ProductOptionsResponse();
            foreach(ProductOption opt in options) {
                response.Items.Add(_mapper.Map<ProductOptionResponse>(opt));
            }
            return response;
        }
    }
}