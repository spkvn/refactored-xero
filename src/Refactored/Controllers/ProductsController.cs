using AutoMapper;
using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Refactored.Data;
using Refactored.Dtos;
using Refactored.Models;

namespace Refactored.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly IProductRepo _repository;
        private readonly IMapper _mapper;

        public ProductsController(IProductRepo repository, IMapper mapper) 
        {
            _repository = repository;
            _mapper = mapper;
        }

        [HttpGet]
        public ActionResult<ProductsResponse> GetProducts([FromQuery(Name = "name")] string name) 
        {
            var products = new List<Product>();
            if (name == null) {
                products = _repository.GetAllProducts().ToList();
            } else {
                products = _repository.GetProductsByName(name).ToList();
            }
            return Ok(prepareProductsResponse(products));
        }

        [HttpGet("{id}", Name="GetProductById")]
        public ActionResult<ProductResponse> GetProductById(Guid id)
        {
            var product = _repository.GetProductById(id);
            if (product == null) {
                return NotFound();
            }
            return Ok(product);
        }

        [HttpPost]
        public ActionResult<ProductResponse> CreateProduct(ProductRequest createRequest)
        {
            var product = _mapper.Map<Product>(createRequest);
            _repository.CreateProduct(product);
            _repository.SaveChanges();
            return CreatedAtRoute(nameof(GetProductById), new {Id = product.Id}, product);
        }

        [HttpPut("{id}")]
        public ActionResult UpdateProduct(Guid id, ProductRequest updateRequest)
        {
            var product = _repository.GetProductById(id);
            if (product == null) 
            {
                return NotFound();
            }
            _mapper.Map(updateRequest, product);
            _repository.SaveChanges();
            return Ok();
        }
        
        [HttpDelete("{id}")]
        public ActionResult DeleteProduct(Guid id)
        {
            var product = _repository.GetProductById(id);
             if (product == null) 
            {
                return NotFound();
            }
            _repository.DeleteProduct(product);
            _repository.SaveChanges();
            return Ok();
        }

        private ProductsResponse prepareProductsResponse(List<Product> products)
        {
            var response = new ProductsResponse();
            foreach(Product product in products) {
                response.Items.Add(_mapper.Map<ProductResponse>(product));
            }
            return response;
        }
    }
}