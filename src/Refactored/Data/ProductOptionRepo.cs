using System;
using System.Collections.Generic;
using Refactored.Models;
using System.Linq;

namespace Refactored.Data
{
    class ProductOptionRepo : IProductOptionRepo
    {
        private readonly ProductContext _context;

        public ProductOptionRepo(ProductContext context)
        {
            _context = context;
        }

        public void CreateProductOption(ProductOption option)
        {
            if (option == null) 
            {
                throw new ArgumentNullException(nameof(option));
            }
            _context.ProductOptions.Add(option);
        }

        public void DeleteProductOption(ProductOption option)
        {
            if (option == null) 
            {
                throw new ArgumentNullException(nameof(option));
            }
            _context.ProductOptions.Remove(option);
        }

        public IEnumerable<ProductOption> GetProductOptionsByProductId(Guid productId)
        {
            if (productId == null) 
            {
                throw new ArgumentNullException(nameof(productId));
            }
            return _context.ProductOptions.Where(po => po.ProductId == productId);
        }

        public ProductOption GetProductOptionsByProductIdAndOptionId(Guid productId, Guid optionId)
        {
            if (productId == null || optionId == null) 
            {
                throw new ArgumentNullException();
            }
            return _context.ProductOptions
                .Where(po => po.ProductId == productId)
                .Where(po => po.Id == optionId)
                .FirstOrDefault();
        }

        public bool SaveChanges()
        {
            return _context.SaveChanges() >= 0;
        }

        public void UpdateProductOption(ProductOption option)
        {
            throw new NotImplementedException();
        }
    }
}