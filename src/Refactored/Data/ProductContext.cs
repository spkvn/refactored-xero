using System;
using Microsoft.EntityFrameworkCore;
using Refactored.Models;

namespace Refactored.Data 
{
    public class ProductContext : DbContext 
    {
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductOption> ProductOptions { get; set; }

        public ProductContext(DbContextOptions<ProductContext> options) : base(options)
        {
        }

        // uncomment for logging
        // protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) 
        //     => optionsBuilder.LogTo(Console.WriteLine);
    }
}