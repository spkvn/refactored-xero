using System;
using System.Collections.Generic;
using System.Linq;
using Refactored.Models;
using Microsoft.EntityFrameworkCore;

namespace Refactored.Data
{
    class ProductRepo : IProductRepo
    {
        private readonly ProductContext _context;

        public ProductRepo(ProductContext context) 
        {
            _context = context;
        }

        public void CreateProduct(Product product) 
        {
            if (product == null) 
            {
                throw new ArgumentNullException(nameof(product));
            }
            _context.Products.Add(product);
        }
        
        public void UpdateProduct(Product product) 
        {
            // no implementation, thanks to how RF context works.
        }
        
        public void DeleteProduct(Product product) 
        {
            if (product == null) 
            {
                throw new ArgumentNullException(nameof(product));
            }
            _context.Products.Remove(product);
        }

        public bool SaveChanges() 
        {
            return _context.SaveChanges() >= 0;
        }
        
        public IEnumerable<Product> GetAllProducts() 
        {
            return _context.Products.ToList();
        }

        public Product GetProductById(Guid id) 
        {
            return _context.Products.FirstOrDefault(p => p.Id == id);
        }

        public IEnumerable<Product> GetProductsByName(string name)
        {
            if (name == null) 
            {
                throw new ArgumentNullException(nameof(name));
            }
            return _context.Products.Where(p => EF.Functions.Like(p.Name.ToLower(),$"%{name.ToLower()}%"));
        }

    }
}