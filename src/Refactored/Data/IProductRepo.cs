using System;
using System.Collections.Generic;
using Refactored.Models;

namespace Refactored.Data
{
    public interface IProductRepo 
    {
        bool SaveChanges();

        IEnumerable<Product> GetAllProducts();
        Product GetProductById(Guid id);

        IEnumerable<Product> GetProductsByName(string name);
        void CreateProduct(Product product);
        void UpdateProduct(Product product);
        void DeleteProduct(Product product);
    }
}