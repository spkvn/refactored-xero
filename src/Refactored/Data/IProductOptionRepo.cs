using System;
using System.Collections.Generic;
using Refactored.Models;

namespace Refactored.Data
{
    public interface IProductOptionRepo
    {
        bool SaveChanges();

        IEnumerable<ProductOption> GetProductOptionsByProductId(Guid productId);

        ProductOption GetProductOptionsByProductIdAndOptionId(Guid productId, Guid optionId);

        void CreateProductOption(ProductOption option);
        void UpdateProductOption(ProductOption option);
        void DeleteProductOption(ProductOption option);

    }
}